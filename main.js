document.addEventListener('DOMContentLoaded', () => {
    const findButton = document.getElementById('findButton');
    const resultDiv = document.getElementById('result');
  
    findButton.addEventListener('click', async () => {
      try {
        const clientIPResponse = await fetch('https://api.ipify.org/?format=json');
        const clientIPData = await clientIPResponse.json();
        const clientIP = clientIPData.ip;
  
        const geoInfoResponse = await fetch(`http://ip-api.com/json/${clientIP}`);
        const geoInfoData = await geoInfoResponse.json();
  
        resultDiv.innerHTML = `
        <p>Code: ${geoInfoData.countryCode}</p>
          <p>Країна: ${geoInfoData.country}</p>
          <p>Регіон: ${geoInfoData.regionName}</p>
          <p>Місто: ${geoInfoData.city}</p>
          <p>Район: ${geoInfoData.district}</p>
        `;
      } catch (error) {
        console.error('Error:', error);
        resultDiv.innerHTML = '<p>Помилка при отриманні інформації</p>';
      }
    });
  });